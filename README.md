# element-web-tweaks

Some scripts to improve the experience of using Element Web.

## How to install

The scripts in this repo are
[userscripts](https://openuserjs.org/about/Userscript-Beginners-HOWTO). Some
browsers can install them without an extension, so it's worth clicking the
"Install" links below.

If that does not work, try installing an extension like
[TamperMonkey](https://www.tampermonkey.net/).

These scripts are developed in Firefox using TamperMonkey, so that setup is
the most likely to be successful. Once TamperMonkey or a similar extension
is installed, try clicking the "Install" link below, and it should work.

## Hide Images On Invitations

**NOTE: I've done my best to make this robust, but this won't protect you from
seeing images if there are incompatible changes in Element Web. Please contact
me if this script needs updating.**

[Install Hide Images On Invitations](https://gitlab.com/andybalaam/element-web-tweaks/-/raw/main/hide-images-on-invitations.user.js)

Attempt to prevent you seeing room avatars on rooms that you are invited to.
This can protect you from seeing images you don't want to see on invite spam,
but it is no guarantee.

If you would like to see this made part of Element Web, I suggest supporting
this issue in the Element Web repository:
[Hide images on invitations](https://github.com/element-hq/element-web/issues/29459).

After installing this script, refresh your Element Web page, and wait a few
seconds. Any images on invitations should be hidden.

If something doesn't work, please log an issue in this repo.

![](screenshot-hide-images-on-invitations.png)

## Minimal Room List

[Install Minimal Room List](https://gitlab.com/andybalaam/element-web-tweaks/-/raw/main/minimal-room-list.user.js)

Replace Element Web's room list with a more minimal one that prioritises
rooms and chats containing unread messages.

After installing this script, refresh your Element Web page, and wait a few
seconds. Your normal room list should be replaced with a more minimal version.

If something doesn't work, please log an issue in this repo.

![](screenshot-minimal-room-list.png)
