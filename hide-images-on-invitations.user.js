// ==UserScript==
// @name         Element Web hide images on invitations
// @namespace    https://gitlab.com/andybalaam/element-web-tweaks/
// @version      2025-03-10
// @description  Remove all images from invitations
// @author       Andy Balaam
// @match        https://*.element.io/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=element.io
// @grant        none
// @license      Apache-2.0
// @copyright    2025, Andy Balaam
// ==/UserScript==

(function() {
    'use strict';

    function addStylesheetRules(rules) {
    // https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleSheet/insertRule
    const styleEl = document.getElementById("ajbStyle") || (function() {
        const s = document.createElement('style');
        s.id = "ajbStyle";
        document.head.appendChild(s);
        return s;
    })();

    var styleSheet = styleEl.sheet;

    for (var i = 0; i < rules.length; i++) {
        var j = 1,
            rule = rules[i],
            selector = rule[0],
            propStr = '';
        // If the second argument of a rule is an array of arrays, correct our variables.
        if (Array.isArray(rule[1][0])) {
            rule = rule[1];
            j = 0;
        }

        for (var pl = rule.length; j < pl; j++) {
            var prop = rule[j];
            propStr += prop[0] + ': ' + prop[1] + (prop[2] ? ' !important' : '') + ';\n';
        }
        styleSheet.insertRule(selector + '{' + propStr + '}', styleSheet.cssRules.length);
    }
}

addStylesheetRules([
    ['.mx_RoomPreviewBar_message img',
        ['display', 'none']
    ],
    ['[aria-labelledby="mx_RoomSublist_label_im.vector.fake.invite"] img',
        ['display', 'none']
    ]
]);
})();